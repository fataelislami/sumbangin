<?php
/**
 * Created by PhpStorm.
 * User: Tandang
 * Date: 06/10/2018
 * Time: 12:40
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function group_by_index($data = array(),$index = '')
    {
        $result = [];
        foreach ($data as $key=>$item){
            $item = (array) $item;
            if (!isset($result[$item[$index]][$key])){
                $result[$item[$index]][$key] = array();
            };
            array_push($result[$item[$index]][$key],$item);
        }
        return $result;
    }
}