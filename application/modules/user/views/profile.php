<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Edit Profile</h4>
                <form class="form-material m-t-40" method="post" action="<?php echo base_url('user/dashboard/update') ?>">
                    <div class="form-group">
                        <label>username</label>
                        <input type="text" name="username" class="form-control" value="<?php echo $dataedit->username?>">
                    </div>
                    <div class="form-group">
                        <label>password</label>
                        <input type="hidden" name="password[]" class="form-control" value="<?php echo $dataedit->password?>">
                        <input type="text" name="password[]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" name="name" class="form-control" value="<?php echo $dataedit->name?>">
                    </div>
                    <div class="form-group">
                        <label>gender</label>
                        <input type="text" name="gender" class="form-control" value="<?php echo $dataedit->gender?>">
                    </div>
                    <div class="form-group">
                        <label>city</label>
                        <input type="text" name="city" class="form-control" value="<?php echo $dataedit->city?>">
                    </div>
                    <div class="form-group">
                        <label>bitrthdate</label>
                        <input type="text" name="bitrthdate" class="form-control" value="<?php echo $dataedit->bitrthdate?>">
                    </div>
                    <div class="form-group">
                        <label>phone</label>
                        <input type="text" name="phone" class="form-control" value="<?php echo $dataedit->phone?>">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
