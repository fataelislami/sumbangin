<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah User</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
	  <div class="form-group">
                    <label>id_user</label>
                    <input type="text" name="id_user" class="form-control" placeholder="" value="<?php echo $dataedit->id_user?>" readonly>
            </div>
	  <div class="form-group">
            <label>username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $dataedit->username?>">
    </div>
	  <div class="form-group">
            <label>password</label>
            <input type="text" name="password" class="form-control" value="<?php echo $dataedit->password?>">
    </div>
	  <div class="form-group">
            <label>name</label>
            <input type="text" name="name" class="form-control" value="<?php echo $dataedit->name?>">
    </div>
	  <div class="form-group">
            <label>gender</label>
            <input type="text" name="gender" class="form-control" value="<?php echo $dataedit->gender?>">
    </div>
	  <div class="form-group">
            <label>city</label>
            <input type="text" name="city" class="form-control" value="<?php echo $dataedit->city?>">
    </div>
	  <div class="form-group">
            <label>latitude</label>
            <input type="text" name="latitude" class="form-control" value="<?php echo $dataedit->latitude?>">
    </div>
	  <div class="form-group">
            <label>longitude</label>
            <input type="text" name="longitude" class="form-control" value="<?php echo $dataedit->longitude?>">
    </div>
	  <div class="form-group">
            <label>bitrthdate</label>
            <input type="text" name="bitrthdate" class="form-control" value="<?php echo $dataedit->bitrthdate?>">
    </div>
	  <div class="form-group">
            <label>phone</label>
            <input type="text" name="phone" class="form-control" value="<?php echo $dataedit->phone?>">
    </div>
	  <div class="form-group">
            <label>flag</label>
            <input type="text" name="flag" class="form-control" value="<?php echo $dataedit->flag?>">
    </div>
	  <div class="form-group">
            <label>chat</label>
            <input type="text" name="chat" class="form-control" value="<?php echo $dataedit->chat?>">
    </div>
	  <div class="form-group">
            <label>counter</label>
            <input type="text" name="counter" class="form-control" value="<?php echo $dataedit->counter?>">
    </div>
	  <div class="form-group">
            <label>joined_date</label>
            <input type="text" name="joined_date" class="form-control" value="<?php echo $dataedit->joined_date?>">
    </div>
	
                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
