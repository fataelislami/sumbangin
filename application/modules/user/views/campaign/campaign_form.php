<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Campaign</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
	  <div class="form-group">
            <label>name</label>
            <input type="text" name="name" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>desc</label>
            <input type="text" name="desc" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>contact</label>
            <input type="text" name="contact" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>datetime</label>
            <input type="text" name="datetime" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>city</label>
            <input type="text" name="city" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>latitude</label>
            <input type="text" name="latitude" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>longitude</label>
            <input type="text" name="longitude" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>verification</label>
            <input type="text" name="verification" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>category</label>
            <input type="text" name="category" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>id_admin</label>
            <input type="text" name="id_admin" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>id_user</label>
            <input type="text" name="id_user" class="form-control" placeholder="">
    </div>
	    <input type="hidden" name="id_campaign" /> 
	
                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
