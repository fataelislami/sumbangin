<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li class="nav-small-cap">Menu</li>
        <!-- //BATAS SATU MENU -->
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>user/campaign"><i class="mdi mdi-gauge"></i><span class="hide-menu">Campaign</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>user/campaign_things"><i class="mdi mdi-gauge"></i><span class="hide-menu">Campaign Donations</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>user/gogreen"><i class="mdi mdi-gauge"></i><span class="hide-menu">Go Green</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>user/gogreen_things"><i class="mdi mdi-gauge"></i><span class="hide-menu">Gogreen Donations</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>user/dashboard/profile"><i class="mdi mdi-gauge"></i><span class="hide-menu">Profile</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>user/dashboard/logout"><i class="mdi mdi-gauge"></i><span class="hide-menu">Logout</span></a>
        </li>
        <!-- //BATAS SATU MENU -->



        <!-- <li>
        CONTOH MENU DENGAN SUBMENU
            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-pencil"></i><span class="hide-menu">Tulisan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="<?php echo base_url()?>admin/tulisan">Tulisan Saya</a></li>
                <li><a href="<?php echo base_url()?>admin/tulisan/all">Tulisan Creator</a></li>
            </ul>
        </li>
         -->
<!-- REFERENSI IKON UNTUK MENU : https://cdn.materialdesignicons.com/1.1.34/ -->
    </ul>
</nav>
