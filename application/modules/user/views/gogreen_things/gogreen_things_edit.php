<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Gogreen_things</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
	  <div class="form-group">
                    <label>id_things</label>
                    <input type="text" name="id_things" class="form-control" placeholder="" value="<?php echo $dataedit->id_things?>" readonly>
            </div>
	  <div class="form-group">
            <label>id_gogreen</label>
            <input type="text" name="id_gogreen" class="form-control" value="<?php echo $dataedit->id_gogreen?>">
    </div>
	  <div class="form-group">
            <label>id_user</label>
            <input type="text" name="id_user" class="form-control" value="<?php echo $dataedit->id_user?>">
    </div>
	  <div class="form-group">
            <label>contents</label>
            <input type="text" name="contents" class="form-control" value="<?php echo $dataedit->contents?>">
    </div>
	  <div class="form-group">
            <label>size</label>
            <input type="text" name="size" class="form-control" value="<?php echo $dataedit->size?>">
    </div>
	  <div class="form-group">
            <label>imageUrl</label>
            <input type="text" name="imageUrl" class="form-control" value="<?php echo $dataedit->imageUrl?>">
    </div>
	  <div class="form-group">
            <label>latitude</label>
            <input type="text" name="latitude" class="form-control" value="<?php echo $dataedit->latitude?>">
    </div>
	  <div class="form-group">
            <label>longitude</label>
            <input type="text" name="longitude" class="form-control" value="<?php echo $dataedit->longitude?>">
    </div>
	  <div class="form-group">
            <label>phone</label>
            <input type="text" name="phone" class="form-control" value="<?php echo $dataedit->phone?>">
    </div>
	  <div class="form-group">
            <label>verification</label>
            <input type="text" name="verification" class="form-control" value="<?php echo $dataedit->verification?>">
    </div>
	
                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
