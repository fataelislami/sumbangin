<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gogreen_things extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Gogreen_things_model');
        $this->load->library('form_validation');
    }

    public function index()
    {

    $this->db->where('id_user', $_SESSION['id']);
    $datagogreen_things=$this->db->get('gogreen_things')->result();//panggil ke modell

      $datafield=$this->Gogreen_things_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'user/gogreen_things/gogreen_things_list',
        'sidebar'=>'user/sidebar',
        'css'=>'user/crudassets/css',
        'script'=>'user/crudassets/script',
        'datagogreen_things'=>$datagogreen_things,
        'datafield'=>$datafield,
        'module'=>'user',
        'titlePage'=>'gogreen_things'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'user/gogreen_things/gogreen_things_form',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/gogreen_things/create_action',
        'titlePage'=>'Gogreen Donations'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Gogreen_things_model->get_by_id($id);
      $data = array(
        'contain_view' => 'user/gogreen_things/gogreen_things_edit',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/gogreen_things/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Gogreen Donations'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_gogreen' => $this->input->post('id_gogreen',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'contents' => $this->input->post('contents',TRUE),
		'size' => $this->input->post('size',TRUE),
		'imageUrl' => $this->input->post('imageUrl',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'verification' => $this->input->post('verification',TRUE),
	    );

            $this->Gogreen_things_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user/gogreen_things'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_things', TRUE));
        } else {
            $data = array(
		'id_gogreen' => $this->input->post('id_gogreen',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'contents' => $this->input->post('contents',TRUE),
		'size' => $this->input->post('size',TRUE),
		'imageUrl' => $this->input->post('imageUrl',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'verification' => $this->input->post('verification',TRUE),
	    );

            $this->Gogreen_things_model->update($this->input->post('id_things', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('user/gogreen_things'));
        }
    }

    public function delete($id)
    {
        $row = $this->Gogreen_things_model->get_by_id($id);

        if ($row) {
            $this->Gogreen_things_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user/gogreen_things'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user/gogreen_things'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('id_gogreen', 'id gogreen', 'trim|required');
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('contents', 'contents', 'trim|required');
	$this->form_validation->set_rules('size', 'size', 'trim|required');
	$this->form_validation->set_rules('imageUrl', 'imageurl', 'trim|required');
	$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
	$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');
	$this->form_validation->set_rules('phone', 'phone', 'trim|required');
	$this->form_validation->set_rules('verification', 'verification', 'trim|required');

	$this->form_validation->set_rules('id_things', 'id_things', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}