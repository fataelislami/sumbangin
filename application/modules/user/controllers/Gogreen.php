<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gogreen extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Gogreen_model');
        $this->load->library('form_validation');
    }

    function index()
    {
        $this->db->where('id_user', $_SESSION['id']);

      $datagogreen=$this->db->get('gogreen')->result();//panggil ke modell
      $datafield=$this->Gogreen_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'user/gogreen/gogreen_list',
        'sidebar'=>'user/sidebar',
        'css'=>'user/crudassets/css',
        'script'=>'user/crudassets/script',
        'datagogreen'=>$datagogreen,
        'datafield'=>$datafield,
        'module'=>'user',
        'titlePage'=>'gogreen'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'user/gogreen/gogreen_form',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/gogreen/create_action',
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Gogreen_model->get_by_id($id);
      $data = array(
        'contain_view' => 'user/gogreen/gogreen_edit',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/gogreen/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'contact' => $this->input->post('contact',TRUE),
		'datetime' => $this->input->post('datetime',TRUE),
		'city' => $this->input->post('city',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		'status' => $this->input->post('status',TRUE),
		'category' => $this->input->post('category',TRUE),
		'id_admin' => $this->input->post('id_admin',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
	    );

            $this->Gogreen_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user/gogreen'));
        }
    }



    public function update_action()
    {
        $this->_rules();

//        if ($this->form_validation->run() == FALSE) {
//            var_dump('e');exit();
//            $this->edit($this->input->post('id_gogreen', TRUE));
//        } else {
            $data = $this->input->post();
            $this->db->set($data);
            $this->db->where('id_gogreen',$data['id_gogreen']);
            $this->Gogreen_model->update($this->input->post('id_gogreen', TRUE), $data);
//            var_dump($this->db->last_query());exit();
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('user/gogreen'));
//        }
    }

    public function delete($id)
    {
        $row = $this->Gogreen_model->get_by_id($id);

        if ($row) {
            $this->Gogreen_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user/gogreen'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user/gogreen'));
        }
    }

    public function _rules()
    {
//	$this->form_validation->set_rules('name', 'name', 'trim|required');
//	$this->form_validation->set_rules('desc', 'desc', 'trim|required');
//	$this->form_validation->set_rules('contact', 'contact', 'trim|required');
//	$this->form_validation->set_rules('datetime', 'datetime', 'trim|required');
//	$this->form_validation->set_rules('city', 'city', 'trim|required');
//	$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
//	$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');
//	$this->form_validation->set_rules('verification', 'verification', 'trim|required');
//	$this->form_validation->set_rules('status', 'status', 'trim|required');
//	$this->form_validation->set_rules('category', 'category', 'trim|required');
//	$this->form_validation->set_rules('id_admin', 'id admin', 'trim|required');
//	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
//
//	$this->form_validation->set_rules('id_gogreen', 'id_gogreen', 'trim');
//	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function doVerified($id){
        $data = array('verification' => 'success');
        $this->Gogreen_model->update($id,$data);
        $this->session->set_flashdata('verification', $id);
        redirect('user/Gogreen');
    }
}