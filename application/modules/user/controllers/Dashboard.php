<?php
/**
 * Created by PhpStorm.
 * User: Tandang
 * Date: 07/10/2018
 * Time: 8:37
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Campaign_model');
        $this->load->model('Gogreen_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data = array(
            'contain_view' => 'user/home_v',
            'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
            'css'=>'user/assets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
            'script'=>'user/assets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
            'titlePage'=>'Dashboard User',//Ini Judul Page untuk tiap halaman
        );
        // $this->load->view('home_v', $data);
        $this->template->load($data);
    }
    function profile($id = null){
        $this->db->where('id_user',$id = $_SESSION['id']);
        $dataedit = $this->db->get('user')->result();

        $data = array(
            'contain_view' => 'user/profile',
            'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
            'css'=>'user/assets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
            'script'=>'user/assets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
            'dataedit'=>$dataedit[0],
            'titlePage'=>'Dashboard User',//Ini Judul Page untuk tiap halaman
        );
//        var_dump($dataedit);exit();
        // $this->load->view('home_v', $data);
        $this->template->load($data);
    }
    function update(){
        $data = $this->input->post();
        $this->_doUpdate($data);
    }
    function _doUpdate($data){

        if (!empty($data['password'][1])){
            $data['password'] = $data['password'][1];
        }else{
            $data['password'] = $data['password'][0];
        }
        $this->db->set($data);
        $this->db->where('id_user',$_SESSION['id']);
        $this->db->update('user');
//        var_dump($this->db->last_query());exit();
        redirect('user/dashboard');
    }
    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }
}