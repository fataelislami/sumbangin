<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Campaign_things extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Campaign_things_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->db->where('id_user', $_SESSION['id']);
        $datacampaign_things=$this->db->get('campaign_things')->result();//panggil ke modell
      $datafield=$this->Campaign_things_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'user/campaign_things/campaign_things_list',
        'sidebar'=>'user/sidebar',
        'css'=>'user/crudassets/css',
        'script'=>'user/crudassets/script',
        'datacampaign_things'=>$datacampaign_things,
        'datafield'=>$datafield,
        'module'=>'user',
        'titlePage'=>'campaign_things'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'user/campaign_things/campaign_things_form',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/campaign_things/create_action',
        'titlePage'=>'Campaign Things'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Campaign_things_model->get_by_id($id);
      $data = array(
        'contain_view' => 'user/campaign_things/campaign_things_edit',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/campaign_things/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Campaign Things'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_campaign' => $this->input->post('id_campaign',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'contents' => $this->input->post('contents',TRUE),
		'size' => $this->input->post('size',TRUE),
		'imageUrl' => $this->input->post('imageUrl',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'verification' => $this->input->post('verification',TRUE),
	    );

            $this->Campaign_things_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user/campaign_things'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_things', TRUE));
        } else {
            $data = array(
		'id_campaign' => $this->input->post('id_campaign',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'contents' => $this->input->post('contents',TRUE),
		'size' => $this->input->post('size',TRUE),
		'imageUrl' => $this->input->post('imageUrl',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'verification' => $this->input->post('verification',TRUE),
	    );

            $this->Campaign_things_model->update($this->input->post('id_things', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('user/campaign_things'));
        }
    }

    public function delete($id)
    {
        $row = $this->Campaign_things_model->get_by_id($id);

        if ($row) {
            $this->Campaign_things_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user/campaign_things'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user/campaign_things'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('id_campaign', 'id campaign', 'trim|required');
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('contents', 'contents', 'trim|required');
	$this->form_validation->set_rules('size', 'size', 'trim|required');
	$this->form_validation->set_rules('imageUrl', 'imageurl', 'trim|required');
	$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
	$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');
	$this->form_validation->set_rules('phone', 'phone', 'trim|required');
	$this->form_validation->set_rules('verification', 'verification', 'trim|required');

	$this->form_validation->set_rules('id_things', 'id_things', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}