<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Campaign extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Campaign_model');
        $this->load->library('form_validation');

    }

    public function index()
    {
        $this->db->where('id_user', $_SESSION['id']);
      $datacampaign=$this->db->get('campaign')->result();//panggil ke modell
//        var_dump($_SESSION);exit();
//        var_dump(json_encode(group_by_index($datacampaign,'category')));exit();
      $datafield=$this->Campaign_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'user/campaign/campaign_list',
        'sidebar'=>'user/sidebar',
        'css'=>'user/crudassets/css',
        'script'=>'user/crudassets/script',
        'datacampaign'=>$datacampaign,
        'datafield'=>$datafield,
        'module'=>'user',
        'titlePage'=>'campaign'
       );
      $this->template->load($data);
    }

    public function create(){
      $data = array(
        'contain_view' => 'user/campaign/campaign_form',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/campaign/create_action',
        'titlePage'=>'Campaign'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Campaign_model->get_by_id($id);
      $data = array(
        'contain_view' => 'user/campaign/campaign_edit',
        'sidebar'=>'user/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'user/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'user/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'user/campaign/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Campaign'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'contact' => $this->input->post('contact',TRUE),
		'datetime' => $this->input->post('datetime',TRUE),
		'city' => $this->input->post('city',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		'category' => $this->input->post('category',TRUE),
		'id_admin' => $this->input->post('id_admin',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
	    );

            $this->Campaign_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user/campaign'));
        }
    }



    public function update_action()
    {
        $data = $this->input->post();
        $this->db->set($data);
        $this->db->where('id_campaign',$data['id_campaign']);
        $this->Campaign_model->update($this->input->post('id_campaign', TRUE), $data);
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('user/campaign'));

    }

    public function delete($id)
    {
        $row = $this->Campaign_model->get_by_id($id);

        if ($row) {
            $this->Campaign_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user/campaign'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user/campaign'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('desc', 'desc', 'trim|required');
	$this->form_validation->set_rules('contact', 'contact', 'trim|required');
	$this->form_validation->set_rules('datetime', 'datetime', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');
	$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
	$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');
	$this->form_validation->set_rules('verification', 'verification', 'trim|required');
	$this->form_validation->set_rules('category', 'category', 'trim|required');
	$this->form_validation->set_rules('id_admin', 'id admin', 'trim|required');
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');

	$this->form_validation->set_rules('id_campaign', 'id_campaign', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function doVerified($id){
        $data = array('verification' => 'success');
        $this->Campaign_model->update($id,$data);
        $this->session->set_flashdata('verification', $id);
        redirect('user/campaign');
    }
}