<?php

class Image{
    function resize($image, $width, $height,$newWidth,$newHeight) {
        //$this->helper('file');                   why need this?
        //$image_content = read_file($image);      We does not want to use this as output.

        //resize image           
        $image = imagecreatefrompng($image);
        $thumbImage = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($thumbImage, $image, 0, 0, 0, 0, $newWidth , $newHeight, $width, $height);
        imagedestroy($image);
        //imagedestroy($thumbImage); do not destroy before display :)
        ob_end_clean();  // clean the output buffer ... if turned on.
        header('Content-Type: image/png');
        imagepng($thumbImage); //you does not want to save.. just display
        imagedestroy($thumbImage); //but not needed, cause the script exit in next line and free the used memory
        exit;
  }

  function resizejpg($image, $width, $height,$newWidth,$newHeight) {
        //$this->helper('file');                   why need this?
        //$image_content = read_file($image);      We does not want to use this as output.

        //resize image           
        $image = imagecreatefromjpeg($image);
        $thumbImage = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($thumbImage, $image, 0, 0, 0, 0, $newWidth , $newHeight, $width, $height);
        imagedestroy($image);
        //imagedestroy($thumbImage); do not destroy before display :)
        ob_end_clean();  // clean the output buffer ... if turned on.
        header('Content-Type: image/jpeg');
        imagepng($thumbImage); //you does not want to save.. just display
        imagedestroy($thumbImage); //but not needed, cause the script exit in next line and free the used memory
        exit;
  }

}