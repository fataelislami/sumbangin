<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jubalapak extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jubalapak_model');
        $this->load->library('form_validation');
    }

    public function index()
    {

      $datajubalapak=$this->Jubalapak_model->get_all();//panggil ke modell
      $datafield=$this->Jubalapak_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'admin/jubalapak/jubalapak_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        'datajubalapak'=>$datajubalapak,
        'datafield'=>$datafield,
        'module'=>'admin',
        'titlePage'=>'jubalapak'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'admin/jubalapak/jubalapak_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/jubalapak/create_action',
        'titlePage'=>'Jubalapak'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Jubalapak_model->get_by_id($id);
      $data = array(
        'contain_view' => 'admin/jubalapak/jubalapak_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/jubalapak/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Jubalapak'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'category' => $this->input->post('category',TRUE),
		'price' => $this->input->post('price',TRUE),
		'contact' => $this->input->post('contact',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		'status' => $this->input->post('status',TRUE),
		'urlImage' => $this->input->post('urlImage',TRUE),
	    );

            $this->Jubalapak_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/jubalapak'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_jubalapak', TRUE));
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'category' => $this->input->post('category',TRUE),
		'price' => $this->input->post('price',TRUE),
		'contact' => $this->input->post('contact',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		'status' => $this->input->post('status',TRUE),
		'urlImage' => $this->input->post('urlImage',TRUE),
	    );

            $this->Jubalapak_model->update($this->input->post('id_jubalapak', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/jubalapak'));
        }
    }

    public function delete($id)
    {
        $row = $this->Jubalapak_model->get_by_id($id);

        if ($row) {
            $this->Jubalapak_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/jubalapak'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/jubalapak'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('desc', 'desc', 'trim|required');
	$this->form_validation->set_rules('category', 'category', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required');
	$this->form_validation->set_rules('contact', 'contact', 'trim|required');
	$this->form_validation->set_rules('verification', 'verification', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('urlImage', 'urlimage', 'trim|required');

	$this->form_validation->set_rules('id_jubalapak', 'id_jubalapak', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}