<?php
/**
 * Created by PhpStorm.
 * User: Tandang
 * Date: 06/10/2018
 * Time: 13:07
 */

class Things_model extends CI_Model
{

    public $table = 'campaign_things';
    public $id = 'id_things';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    //get field
    function get_field(){
        return $this->db->list_fields($this->table);
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_things', $q);
        $this->db->or_like('id_campaign', $q);
        $this->db->or_like('id_user', $q);
        $this->db->or_like('contents', $q);
        $this->db->or_like('size', $q);
        $this->db->or_like('imageUrl', $q);
        $this->db->or_like('latitude', $q);
        $this->db->or_like('longitude', $q);
        $this->db->or_like('phone', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_things', $q);
        $this->db->or_like('id_campaign', $q);
        $this->db->or_like('id_user', $q);
        $this->db->or_like('contents', $q);
        $this->db->or_like('size', $q);
        $this->db->or_like('imageUrl', $q);
        $this->db->or_like('latitude', $q);
        $this->db->or_like('longitude', $q);
        $this->db->or_like('phone', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}