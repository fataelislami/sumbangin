<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Jubalapak</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
	  <div class="form-group">
                    <label>id_jubalapak</label>
                    <input type="text" name="id_jubalapak" class="form-control" placeholder="" value="<?php echo $dataedit->id_jubalapak?>" readonly>
            </div>
	  <div class="form-group">
            <label>id_user</label>
            <input type="text" name="id_user" class="form-control" value="<?php echo $dataedit->id_user?>">
    </div>
	  <div class="form-group">
            <label>name</label>
            <input type="text" name="name" class="form-control" value="<?php echo $dataedit->name?>">
    </div>
	  <div class="form-group">
            <label>desc</label>
            <input type="text" name="desc" class="form-control" value="<?php echo $dataedit->desc?>">
    </div>
	  <div class="form-group">
            <label>category</label>
            <input type="text" name="category" class="form-control" value="<?php echo $dataedit->category?>">
    </div>
	  <div class="form-group">
            <label>price</label>
            <input type="text" name="price" class="form-control" value="<?php echo $dataedit->price?>">
    </div>
	  <div class="form-group">
            <label>contact</label>
            <input type="text" name="contact" class="form-control" value="<?php echo $dataedit->contact?>">
    </div>
	  <div class="form-group">
            <label>verification</label>
            <input type="text" name="verification" class="form-control" value="<?php echo $dataedit->verification?>">
    </div>
	  <div class="form-group">
            <label>status</label>
            <input type="text" name="status" class="form-control" value="<?php echo $dataedit->status?>">
    </div>
	  <div class="form-group">
            <label>urlImage</label>
            <input type="text" name="urlImage" class="form-control" value="<?php echo $dataedit->urlImage?>">
    </div>
	
                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
