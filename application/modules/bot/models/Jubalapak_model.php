<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jubalapak_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function totalProduct($category){
    $sql="SELECT count(`id_jubalapak`) as `jumlah` from `jubalapak` where `category`='$category'";
    return $this->db->query($sql)->row()->jumlah;
  }

  function statusCounter($status,$category){
    $sql="SELECT count(`id_jubalapak`) as `jumlah` from `jubalapak` where `status`='$status' and `category`='$category' and `verification`='success'";
    return $this->db->query($sql)->row()->jumlah;
  }

}
