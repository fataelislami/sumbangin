<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function totalSumbangers($category){
    $sql="SELECT count(`id_campaign`) as `jumlah` from `campaign` where `category`='$category'";
    return $this->db->query($sql)->row()->jumlah;
  }

  function statusCounter($status,$category){
    $sql="SELECT count(`id_campaign`) as `jumlah` from `campaign` where `status`='$status' and `category`='$category' and `verification`='success'";
    return $this->db->query($sql)->row()->jumlah;
  }

}
