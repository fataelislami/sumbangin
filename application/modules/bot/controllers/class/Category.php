<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('MessageBuilder.php');
require_once('Flex.php');
class Category extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs','Campaign_model','Gogreen_model','Jubalapak_model'));
  }

  function index()
  {

  }
  function itemJubalapak($category){
    $flex=new Flex();
    $send=new MessageBuilder();
    $conditions=array('category'=>$category,'status'=>'sell','verification'=>'success');
    $loadDb=$this->Dbs->getdata($conditions,'jubalapak');
    $check=$loadDb->num_rows();//cek jumlah rows yang akan muncul berdasarkan kondisi
    if($check>0){
      $getData=$loadDb->result();
      $limit=1;
      $contents=[];//menyiapkan array untuk caraousel
      $image=base_url().'xfile/images/dummy.jpg';
      foreach ($getData as $g) {
        if($g->urlImage!=null){
          $image=base_url().'xfile/images/'.$g->urlImage;
        }
        $item=$flex->itemJubalapak($g->id_jubalapak,$g->name,$g->desc,$g->price,$image);//phone masih dari table user
        array_push($contents,$item);
        if($limit>10){
            break;
        }
        $limit++;
      }
      $finalcontents=array (//memasukan hasil loop array ke array utama
          'type' => 'carousel',
          'contents' =>$contents
          );
          $messages=[];
          $msg1=$send->text("List Barang");
          $msg2=$send->flex("Barang",$finalcontents);//memasukan array utama ke builder flex message
          array_push($messages,$msg1,$msg2);

    }else{
      $messages=[];
      $msg1=$send->text("Ups belum ada barang untuk kategori ini");
      array_push($messages,$msg1);
    }
    return $messages;
  }
  function itemSumbangan($category){
    $flex=new Flex();
    $send=new MessageBuilder();
    $conditions=array('category'=>$category,'status'=>'onprogress','verification'=>'success');
    $loadDb=$this->Dbs->getdata($conditions,'campaign');
    $check=$loadDb->num_rows();//cek jumlah rows yang akan muncul berdasarkan kondisi
    if($check>0){
      $getData=$loadDb->result();
      $limit=1;
      $contents=[];//menyiapkan array untuk caraousel
      $image=base_url().'xfile/images/dummy.jpg';
      foreach ($getData as $g) {
        $loadImg=$this->Dbs->getdata(array('id_campaign'=>$g->id_campaign),'campaign_image');
        if($loadImg->num_rows()>0){
          $image=base_url().'xfile/images/'.$loadImg->row()->name;
        }
        $item=$flex->itemCategory($g->id_campaign,$g->name,$g->desc,$g->contact,'campaign',$image);//phone masih dari table user
        array_push($contents,$item);
        if($limit>10){
            break;
        }
        $limit++;
      }
      $finalcontents=array (//memasukan hasil loop array ke array utama
          'type' => 'carousel',
          'contents' =>$contents
          );
          $messages=[];
          $msg1=$send->text("List Kampanye");
          $msg2=$send->flex("Kampanye",$finalcontents);//memasukan array utama ke builder flex message
          array_push($messages,$msg1,$msg2);

    }else{
      $messages=[];
      $msg1=$send->text("Ups belum ada kampanye untuk kategori ini");
      array_push($messages,$msg1);
    }
    return $messages;
  }

  function itemGogreen($category){
    $flex=new Flex();
    $send=new MessageBuilder();
    $conditions=array('category'=>$category,'status'=>'onprogress','verification'=>'success');
    $loadDb=$this->Dbs->getdata($conditions,'gogreen');
    $check=$loadDb->num_rows();//cek jumlah rows yang akan muncul berdasarkan kondisi
    if($check>0){
      $getData=$loadDb->result();
      $limit=1;
      $contents=[];//menyiapkan array untuk caraousel
      $image=base_url().'xfile/images/dummy.jpg';
      foreach ($getData as $g) {
        $loadImg=$this->Dbs->getdata(array('id_gogreen'=>$g->id_gogreen),'gogreen_image');
        if($loadImg->num_rows()>0){
          $image=base_url().'xfile/images/'.$loadImg->row()->name;
        }
        $item=$flex->itemCategory($g->id_gogreen,$g->name,$g->desc,$g->contact,'gogreen',$image);//phone masih dari table user
        array_push($contents,$item);
        if($limit>10){
            break;
        }
        $limit++;
      }
      $finalcontents=array (//memasukan hasil loop array ke array utama
          'type' => 'carousel',
          'contents' =>$contents
          );
          $messages=[];
          $msg1=$send->text("List Kampanye");
          $msg2=$send->flex("Kampanye",$finalcontents);//memasukan array utama ke builder flex message
          array_push($messages,$msg1,$msg2);

    }else{
      $messages=[];
      $msg1=$send->text("Ups belum ada kampanye untuk kategori ini");
      array_push($messages,$msg1);
    }
    return $messages;
  }

  function sumbangan(){
    $title = array('keluarga','bencana','disabilitas','pendidikan','rumah ibadah','kampanye sosial');
    $flex=new Flex();
    $send=new MessageBuilder();
    $contents=[];//menyiapkan array untuk caraousel
    $limit=1;
    foreach ($title as $t) {
      $jumlah=$this->Campaign_model->totalSumbangers($t);//mengambil jumlah row dengan where category=$t
      $berlangsung=$this->Campaign_model->statusCounter('onprogress',$t);
      $selesai=$this->Campaign_model->statusCounter('finish',$t);
      switch ($t) {
          case "keluarga":
              $image="fleximage/keluarga.jpg";
              break;
          case "bencana":
              $image="fleximage/bencana_alam.jpg";
              break;
          case "disabilitas":
              $image="fleximage/disabilitas.jpg";
              break;
          case "pendidikan":
              $image="fleximage/pendidikan.jpg";
              break;
          case "rumah ibadah":
              $image="fleximage/rumah_ibadah.jpg";
              break;
          case "kampanye sosial":
              $image="fleximage/kampanye_sosial.jpg";
              break;
          default:
              $image="images/dummy.jpg";
      }
      $item=$flex->category($t,$jumlah,$berlangsung,$selesai,'campaign',base_url().'xfile/'.$image);
      array_push($contents,$item);
      if($limit>10){
          break;
      }
      $limit++;
    }
    $finalcontents=array (
        'type' => 'carousel',
        'contents' =>$contents
        );
        $messages=[];
        $msg1=$send->text("List Kategori Sumbangin");
        $msg2=$send->flex("Kategori",$finalcontents);
        array_push($messages,$msg1,$msg2);
        return $messages;

  }

  function gogreen(){
    $title = array('kertas','plastik');
    $flex=new Flex();
    $send=new MessageBuilder();
    $contents=[];//menyiapkan array untuk caraousel
    $limit=1;
    foreach ($title as $t) {
      $jumlah=$this->Gogreen_model->totalSumbangers($t);//mengambil jumlah row dengan where category=$t
      $berlangsung=$this->Gogreen_model->statusCounter('onprogress',$t);
      $selesai=$this->Gogreen_model->statusCounter('finish',$t);
      switch ($t) {
          case "plastik":
              $image="fleximage/plastik.jpg";
              break;
          case "kertas":
              $image="fleximage/kertas.jpg";
              break;
          default:
              $image="images/dummy.jpg";
      }
      $item=$flex->category($t,$jumlah,$berlangsung,$selesai,'gogreen',base_url().'xfile/'.$image);
      array_push($contents,$item);
      if($limit>10){
          break;
      }
      $limit++;
    }
    $finalcontents=array (
        'type' => 'carousel',
        'contents' =>$contents
        );
        $messages=[];
        $msg1=$send->text("List Kategori GoGreen");
        $msg2=$send->flex("GoGreen",$finalcontents);
        array_push($messages,$msg1,$msg2);
        return $messages;

  }

  function jubalapak(){
    $title = array('Pakaian Pria','Aksesoris','Pakaian Wanita','Sepatu','Lainnya');
    $flex=new Flex();
    $send=new MessageBuilder();
    $contents=[];//menyiapkan array untuk caraousel
    $limit=1;
    $image="images/dummy.jpg";
    foreach ($title as $t) {
      $jumlah=$this->Jubalapak_model->totalProduct($t);//mengambil jumlah row dengan where category=$t
      $sell=$this->Jubalapak_model->statusCounter('sell',$t);
      $sold=$this->Jubalapak_model->statusCounter('sold',$t);
      // switch ($t) {
      //     case "plastik":
      //         $image="fleximage/plastik.jpg";
      //         break;
      //     case "kertas":
      //         $image="fleximage/kertas.jpg";
      //         break;
      //     default:
      //         $image="images/dummy.jpg";
      // }
      $item=$flex->categoryJubalapak($t,$jumlah,$sell,$sold,base_url().'xfile/'.$image);
      array_push($contents,$item);
      if($limit>10){
          break;
      }
      $limit++;
    }
    $finalcontents=array (
        'type' => 'carousel',
        'contents' =>$contents
        );
        $messages=[];
        $msg1=$send->text("List Kategori Jubalapak");
        $msg2=$send->flex("Jubalapak",$finalcontents);
        array_push($messages,$msg1,$msg2);
        return $messages;

  }

}
