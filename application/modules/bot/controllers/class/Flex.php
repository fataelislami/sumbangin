<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flex extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

  }
  function categoryJubalapak($title,$barang=0,$sell,$sold,$image){
    $item=array (
  'type' => 'bubble',
  'hero' =>
  array (
    'type' => 'image',
    'size' => 'full',
    'aspectRatio' => '20:13',
    'aspectMode' => 'cover',
    'url' => $image,
  ),
  'body' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'sm',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'text',
        'text' => $title,
        'wrap' => true,
        'weight' => 'bold',
        'size' => 'xl',
      ),
      1 =>
      array (
        'type' => 'box',
        'layout' => 'baseline',
        'spacing' => 'sm',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => 'Barang Ready',
            'wrap' => true,
            'weight' => 'bold',
            'size' => 'sm',
            'flex' => 1,
          ),
          1 =>
          array (
            'type' => 'icon',
            'size' => 'md',
            'url' => 'https://islamify.id/bot/temuan/assets/icon/followers.png',
          ),
          2 =>
          array (
            'type' => 'text',
            'text' => $barang,
            'wrap' => true,
            'size' => 'sm',
            'flex' => 0,
            'align' => 'end',
          ),
        ),
      ),
      2 =>
      array (
        'type' => 'box',
        'layout' => 'vertical',
        'margin' => 'xxl',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'spacer',
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'Status Jubalapak',
            'weight' => 'bold',
            'color' => '#1DB446',
            'size' => 'sm',
          ),
        ),
      ),
      3 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'margin' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => 'Sell',
            'size' => 'xs',
            'flex' => 0,
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'Sold',
            'size' => 'xs',
            'align' => 'end',
          ),
        ),
      ),
      4 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'margin' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => $sell,
            'size' => 'xs',
            'flex' => 0,
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => $sold,
            'size' => 'xs',
            'align' => 'end',
          ),
        ),
      ),
    ),
  ),
  'footer' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'md',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'button',
        'style' => 'primary',
        'color' => '#487bdc',
        'action' =>
        array (
          'type' => 'postback',
          'label' => 'Lihat',
          'data' => 'jubalapak_category#'.$title,
          'text' => 'lihat',
        ),
      ),
    ),
  ),
);
return $item;
  }
  function category($title,$sumbangers=0,$berlangsung,$selesai,$from,$image){
    $item=array (
  'type' => 'bubble',
  'hero' =>
  array (
    'type' => 'image',
    'size' => 'full',
    'aspectRatio' => '20:13',
    'aspectMode' => 'cover',
    'url' => $image,
  ),
  'body' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'sm',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'text',
        'text' => $title,
        'wrap' => true,
        'weight' => 'bold',
        'size' => 'xl',
      ),
      1 =>
      array (
        'type' => 'box',
        'layout' => 'baseline',
        'spacing' => 'sm',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => 'Sumbangers',
            'wrap' => true,
            'weight' => 'bold',
            'size' => 'sm',
            'flex' => 1,
          ),
          1 =>
          array (
            'type' => 'icon',
            'size' => 'md',
            'url' => 'https://islamify.id/bot/temuan/assets/icon/followers.png',
          ),
          2 =>
          array (
            'type' => 'text',
            'text' => $sumbangers,
            'wrap' => true,
            'size' => 'sm',
            'flex' => 0,
            'align' => 'end',
          ),
        ),
      ),
      2 =>
      array (
        'type' => 'box',
        'layout' => 'vertical',
        'margin' => 'xxl',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'spacer',
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'Status Kampanye',
            'weight' => 'bold',
            'color' => '#1DB446',
            'size' => 'sm',
          ),
        ),
      ),
      3 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'margin' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => 'Berlangsung',
            'size' => 'xs',
            'flex' => 0,
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'Selesai',
            'size' => 'xs',
            'align' => 'end',
          ),
        ),
      ),
      4 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'margin' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => $berlangsung,
            'size' => 'xs',
            'flex' => 0,
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => $selesai,
            'size' => 'xs',
            'align' => 'end',
          ),
        ),
      ),
    ),
  ),
  'footer' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'md',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'button',
        'style' => 'primary',
        'color' => '#487bdc',
        'action' =>
        array (
          'type' => 'postback',
          'label' => 'Lihat',
          'data' => 'category#'.$from.'#'.$title,
          'text' => 'lihat',
        ),
      ),
    ),
  ),
);
return $item;
  }

  function itemCategory($id=null,$title='',$desc='',$phone=0,$from,$image){
    $item=array (
  'type' => 'bubble',
  'hero' =>
  array (
    'type' => 'image',
    'size' => 'full',
    'aspectRatio' => '20:13',
    'aspectMode' => 'cover',
    'url' => $image,
  ),
  'body' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'sm',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'text',
        'text' => $title,
        'wrap' => true,
        'weight' => 'bold',
        'size' => 'md',
      ),
      1 =>
      array (
        'type' => 'box',
        'layout' => 'baseline',
        'spacing' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'icon',
            'size' => 'md',
            'url' => 'https://islamify.id/bot/temuan/assets/icon/followers.png',
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'Submiters',
            'wrap' => true,
            'weight' => 'bold',
            'size' => 'sm',
          ),
          2 =>
          array (
            'type' => 'text',
            'text' => '-',
            'wrap' => true,
            'size' => 'sm',
            'align' => 'end',
          ),
        ),
      ),
      2 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'spacing' => 'sm',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => substr($desc,0,150),
            'wrap' => true,
            'align' => 'start',
            'size' => 'sm',
          ),
        ),
      ),
    ),
  ),
  'footer' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'md',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'button',
        'action' =>
        array (
          'type' => 'postback',
          'label' => 'Selengkapnya',
          'data' => 'readdesc#'.$from."#$id",
          'text' =>'Read More'
        ),
      ),
      1 =>
      array (
        'type' => 'button',
        'style' => 'primary',
        'color' => '#487bdc',
        'action' =>
        array (
          'type' => 'uri',
          'label' => 'Telfon',
          'uri' => 'tel:'.$phone,
        ),
      ),
      2 =>
      array (
        'type' => 'button',
        'style' => 'primary',
        'color' => '#49B2DC',
        'action' =>
        array (
          'type' => 'postback',
          'label' => 'Sumbangin Ke Sini',
          'data' => 'sumbangin#'.$from."#$id",//data yang akan dikirim ke postback
          'text'=>'mulai sumbangin'
        ),
      ),
    ),
  ),
);
return $item;
  }

  function itemJubalapak($id=null,$title='',$desc='',$price,$image){
    $item=array (
  'type' => 'bubble',
  'hero' =>
  array (
    'type' => 'image',
    'size' => 'full',
    'aspectRatio' => '20:13',
    'aspectMode' => 'cover',
    'url' => $image,
  ),
  'body' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'sm',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'text',
        'text' => $title,
        'wrap' => true,
        'weight' => 'bold',
        'size' => 'md',
      ),
      1 =>
      array (
        'type' => 'box',
        'layout' => 'baseline',
        'spacing' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'icon',
            'size' => 'md',
            'url' => 'https://islamify.id/bot/temuan/assets/icon/followers.png',
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'Harga',
            'wrap' => true,
            'weight' => 'bold',
            'size' => 'sm',
          ),
          2 =>
          array (
            'type' => 'text',
            'text' => 'Rp.'.$price,
            'wrap' => true,
            'size' => 'sm',
            'align' => 'end',
          ),
        ),
      ),
      2 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'spacing' => 'sm',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => substr($desc,0,150),
            'wrap' => true,
            'align' => 'start',
            'size' => 'sm',
          ),
        ),
      ),
    ),
  ),
  'footer' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'md',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'button',
        'action' =>
        array (
          'type' => 'postback',
          'label' => 'Selengkapnya',
          'data' => 'readdesc#'.$id,
          'text' =>'Read More'
        ),
      ),
      1 =>
      array (
        'type' => 'button',
        'style' => 'primary',
        'color' => '#487bdc',
        'action' =>
        array (
          'type' => 'uri',
          'label' => 'Order via Sumbangin',
          'uri' => 'line://oaMessage/@stl4871w/?Order%20'.str_replace(" ","%20",$title).'%20no%20produk='.$id
        ),
      ),
    ),
  ),
);
return $item;
  }

}
