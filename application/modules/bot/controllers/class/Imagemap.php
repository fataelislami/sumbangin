<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imagemap extends Bot{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

  }

  function jubalapak_category(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/jubalapak_category/',
  'altText' => 'Jubalapak',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 135,
        'y' => 435,
        'width' => 206,
        'height' => 264,
      ),
      'text' => 'jbl@Pakaian Pria',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 413,
        'y' => 435,
        'width' => 244,
        'height' => 283,
      ),
      'text' => 'jbl@Aksesoris',
    ),
    2 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 678,
        'y' => 433,
        'width' => 268,
        'height' => 297,
      ),
      'text' => 'jbl@Pakaian Wanita',
    ),
    3 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 123,
        'y' => 721,
        'width' => 265,
        'height' => 255,
      ),
      'text' => 'jbl@Sepatu',
    ),
    4 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 678,
        'y' => 733,
        'width' => 277,
        'height' => 248,
      ),
      'text' => 'jbl@Lainnya',
    ),
  ),
);
return $item;
  }

  function box(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/box/',
  'altText' => 'Kardus in',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 114,
        'y' => 293,
        'width' => 335,
        'height' => 374,
      ),
      'text' => 'M@BOX',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 600,
        'y' => 205,
        'width' => 404,
        'height' => 454,
      ),
      'text' => 'XL@BOX',
    ),
    2 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 106,
        'y' => 676,
        'width' => 348,
        'height' => 348,
      ),
      'text' => 'L@BOX',
    ),
    3 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 598,
        'y' => 669,
        'width' => 376,
        'height' => 357,
      ),
      'text' => 'S@BOX',
    ),
  ),
);
return $item;
  }

  function categorygogreen(){//menu untuk gogreen
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/categorygogreen/',
  'altText' => 'Go Green in apa nih Cuy!',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 173,
        'y' => 492,
        'width' => 328,
        'height' => 415,
      ),
      'text' => '!Buat Go Green@Kertas',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 558,
        'y' => 484,
        'width' => 343,
        'height' => 428,
      ),
      'text' => '!Buat Go Green@Plastik',
    ),
  ),
);
return $item;
  }

  function gogreen(){//menu untuk gogreen
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/gogreen/',
  'altText' => 'Go Green Cuy!',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 173,
        'y' => 492,
        'width' => 328,
        'height' => 415,
      ),
      'text' => '!Go Green Kampanye',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 558,
        'y' => 484,
        'width' => 343,
        'height' => 428,
      ),
      'text' => '!Lihat Go Green Kampanye',
    ),
  ),
);
return $item;
  }

  function buka_sumbangan(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/buka_sumbangan/',
  'altText' => 'Buka Sumbangan',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 141,
        'y' => 408,
        'width' => 218,
        'height' => 255,
      ),
      'text' => '!sumbangan@keluarga',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 414,
        'y' => 404,
        'width' => 250,
        'height' => 262,
      ),
      'text' => '!sumbangan@bencana',
    ),
    2 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 695,
        'y' => 397,
        'width' => 229,
        'height' => 272,
      ),
      'text' => '!sumbangan@disabilitas',
    ),
    3 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 140,
        'y' => 674,
        'width' => 222,
        'height' => 255,
      ),
      'text' => '!sumbangan@pendidikan',
    ),
    4 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 416,
        'y' => 674,
        'width' => 246,
        'height' => 257,
      ),
      'text' => '!sumbangan@rumah_ibadah',
    ),
    5 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 690,
        'y' => 678,
        'width' => 243,
        'height' => 268,
      ),
      'text' => '!sumbangan@kempanye_sosial',
    ),
  ),
);
return $item;
  }



  function upload_foto(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/upload_foto/',
  'altText' => 'Upload Foto',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'uri',
      'area' =>
      array (
        'x' => 53,
        'y' => 408,
        'width' => 424,
        'height' => 416,
      ),
      'linkUri' => 'line://nv/camera/	',
    ),
    1 =>
    array (
      'type' => 'uri',
      'area' =>
      array (
        'x' => 585,
        'y' => 398,
        'width' => 428,
        'height' => 422,
      ),
      'linkUri' => 'line://nv/cameraRoll/multi',
    ),
  ),
);
return $item;
  }


  function lokasi(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/location/',
  'altText' => 'Kirimkan Lokasi',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'uri',
      'area' =>
      array (
        'x' => 2,
        'y' => 6,
        'width' => 1030,
        'height' => 1022,
      ),
      'linkUri' => 'line://nv/location',
    ),
  ),
);
return $item;
  }

  function jubalapak(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/jubalapak/',
  'altText' => 'Jubalapak',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 118,
        'y' => 411,
        'width' => 379,
        'height' => 456,
      ),
      'text' => '!Jubalapak Create',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 534,
        'y' => 406,
        'width' => 440,
        'height' => 471,
      ),
      'text' => '!Jubalapak Ready',
    ),
  ),
);
return $item;
  }




}
