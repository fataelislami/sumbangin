<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
  }

  function index()
  {
    header('Content-Type: application/json');

    $data=$this->Dbs->logs()->row();
    echo $data->json;
      # code...
  }

}
