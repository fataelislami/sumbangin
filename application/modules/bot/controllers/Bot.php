<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('line_class.php');
require_once('class/MessageBuilder.php');
require_once('class/Register.php');


class Bot extends MY_Controller {

   /*
        WELCOME TO KOSTLAB X CODEIGNITER FRAMEWORK
        Framework inidibuat untuk memudahkan development chatbot LINE
        Coders : @kostlab @fataelislami


        Dokumentasi Fungsi
                                   function update($where,$data,$to)
                                   function getdata($userid,$from)
                                   function insert($data,$to)

        Struktur Model
            DBS
        Struktur Controller
            Welcome


      Brief Docs
            Set Flag

      Check Flag
      if($db[0]->flag=='blablabla')

      Message Type
      if($message['type']=='location')
      if($message['type']=='text')
      if($message['type']=='image')
      if($message['type']=='audio')
      if($message['type']=='video')

      Event Type
      $event['type'] == 'follow'
      $event['type'] == 'unfollow'
      $event['type'] == 'join'
      $event['type'] == 'leave'




     */

     public function __construct()
          {
            parent::__construct();
            //Codeigniter : Write Less Do More
      $this->load->model(array('Dbs'));


          }


  public function index()
  {

    //Konfigurasi Chatbot
    $channelAccessToken = 'uLDiz75RqMSCaopYzxpmzpDQWmXORrfR4PKZx+OLfuiGvU7YuuxVFFogV6Q7X5bdJf3lQvvAn9heEifieyk8Z1dUf2VoDbvOmdfo6exevxzaYgekTSrfZk/Mxr2O3Bk76qQd1qy3l0rRvpUuAykomQdB04t89/1O/w1cDnyilFU=';
    $channelSecret = '844c3f74b230c4773cc896404ac5bf00';//sesuaikan
    //Konfigurasi Chatbot END

    $client = new LINEBotTiny($channelAccessToken, $channelSecret);
    $send= new MessageBuilder();
    $reg=new Register();

        $userId   = $client->parseEvents()[0]['source']['userId'];
        $groupId    = $client->parseEvents()[0]['source']['groupId'];
        $replyToken = $client->parseEvents()[0]['replyToken'];
        $timestamp  = $client->parseEvents()[0]['timestamp'];
        $message  = $client->parseEvents()[0]['message'];
        $messageid  = $client->parseEvents()[0]['message']['id'];
        $latitude=$client->parseEvents()[0]['message']['latitude'];
        $longitude=$client->parseEvents()[0]['message']['longitude'];
        $address=$client->parseEvents()[0]['message']['address'];
        $addresstitle=$client->parseEvents()[0]['message']['title'];
        $postback=$client->parseEvents() [0]['postback'];
        $profil = $client->profil($userId);
        $nama=$profil->displayName;
        $pesan_datang = $message['text'];
        $upPesan = strtoupper($pesan_datang);
        $pecahnama=explode(" ",$profil->displayName);
        $namapanggilan=$pecahnama[0];
        $event=$client->parseEvents()[0];

        $db = $this->Dbs->getdata(array('id_user'=>$userId), 'user')->row();
        function getRandom($length = 3) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        //Fungsi cek register

        //

            if ($event['type'] == 'follow')//Yang bot lakukan pertama kali saat di add oleh user
              {
                $data=array(
                      'id_user'=>$userId,
                      'name'=>$nama,
                      'flag'=>'register',
                      'counter'=>1
                    );
                    $sql=$this->Dbs->insert($data,'user');
                    if($sql){
                      $buttons=[];
                      $button1 = array('type'=>'postback','label'=>'Cewe','data'=>'gender#female');
                      $button2 = array('type'=>'postback','label'=>'Cowo','data'=>'gender#male');
                      array_push($buttons,$button1,$button2);
                      $messages=[];
                      $msg1=$send->text("Holaa ".$namapanggilan."! sebelum gunain sumbangin, kenalan dulu yuk! kamu itu cewe apa cowo?");
                      $msg2=$send->confirmMessage("Kenalan Yuk!","Jenis Kelamin",$buttons);
                      array_push($messages,$msg1,$msg2);
                      $output = $send->reply($replyToken,$messages);
                    }
              }
              if ($event['type'] == 'unfollow')//Yang bot lakukan pertama kali saat di add oleh user
              {

              }

            if ($event['type'] == 'join')
              {

              }


          //MAPPING FITUR
          if($db->flag=='register' and $groupId==null){
            if(substr($postback['data'],0,6)=='gender' && $db->counter==1){
              $arr=explode("#",$postback['data']);
              $gender=$arr[1];
              $data=array(
                'counter'=>$db->counter+1,
                'gender'=>$gender
              );
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                require_once 'class/Imagemap.php';
                $imagemap=new Imagemap();
                $messages=[];
                $msg1=$send->text('okay! satu tahap lagi nih,untuk menyediakan informasi akurat sumbangin membutuhkan info lokasi, kirimkan lokasi kamu saat ini ya! tap link ini ');
                $msg2=$imagemap->lokasi();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }
            }else if($message['type']=='text' and $db->counter==1){
              $messages=array($send->text("Ups kamu belum mengatur jenis kelamin mu, silahkan tap button jenis kelamin untuk menyelesaikan registrasi"));
              $output=$send->reply($replyToken,$messages);
            }
            if($message['type']=='location' && $db->counter==2){
              require_once 'class/LocationHelper.php';
              $location=new LocationHelper();
              $city=$location->getCity($latitude,$longitude);
              $data=array(
                'city'=>$city,
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'counter'=>0,
                'flag'=>'default'
              );
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                $messages=[];
                $msg1=$send->text('Okay! sumbangin udh kenal kamu nih!, kalo masi bingung silahkan cek Timeline aja yak!');
                array_push($messages,$msg1);
                $output=$send->reply($replyToken,$messages);
              }

            }else if($message['type']=='text' and $db->counter==2){
              $messages=array($send->text("Ups kamu belum memberitahu lokasimu, silahkan tap button lokasi untuk menyelesaikan registrasi"));
              $output=$send->reply($replyToken,$messages);
            }



          }
          else if($db->flag=='phoneUpdate'){
            if($message['type']=='text'){
              $phone=preg_replace('/[^0-9]/', '', $pesan_datang);
              $data['phone']=$phone;
              if($phone!=0){
                $data['flag']='default';
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                if($sql){
                  $message=array($send->text("Nomor telfon berhasil di set, silahkan ulangi perintah"));
                  $output=$send->reply($replyToken,$message);
                }
              }else{
                $message=array($send->text("Ups sepertinya input yang kamu masukan bukan angka"));
                $output=$send->reply($replyToken,$message);
              }
            }

          }
          else if(substr($db->flag,0,9)=='jubalapak'){
            require_once('class/Imagemap.php');
            $imagemap=new Imagemap();
            $getpostdata=explode("#",$db->flag);
            $id_jubalapak=$getpostdata[1];
            $condition=array('id_user'=>$userId,'id_jubalapak'=>$id_jubalapak);
            if($message['type']=='text' and $db->counter==1 and substr($upPesan,0,3) !='JBL'){
              $data['name']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'jubalapak');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $message=array($send->text("Oke! bagaimana deskripsi barang tersebut?"));
                $output=$send->reply($replyToken,$message);
              }
            }
            if($message['type']=='text' and $db->counter==2){
              $data['desc']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'jubalapak');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $message=array($send->text("Berapa harga yang akan anda tawarkan?"));
                $output=$send->reply($replyToken,$message);
              }
            }
            if($message['type']=='text' and $db->counter==3){
              $data['price']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'jubalapak');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $messages=[];
                $msg1=$send->text("Silahkan upload foto barang tersebut");
                $msg2=$imagemap->upload_foto();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }
            }

            if ($message['type']=='image' and $db->counter==4){
              $imgprocess=$this->getMessage($messageid);
              if($imgprocess!="error"){
                $data['urlImage']=$imgprocess;
                $sql=$this->Dbs->update($condition,$data,"jubalapak");
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $messages=[];
                  $msg1=$send->text("Foto berhasil disimpan!,ada kontak yang bisa kami hubungi? balas pesan ini dengan nomor hp ya");
                  array_push($messages,$msg1);
                  $output = $send->reply($replyToken,$messages);
                }
              }else{
                $pre=array($send->text("Gagal Upload,Pastikan Ukuran Foto Tidak Terlalu Besar"));
                $output=$send->reply($replyToken,$pre);
              }

            }else if($message['type']=='text' and $db->counter==4){
              $message=array($send->text("Ups upload dulu minimal 1 Foto untuk melanjutkan jubalapak"));
              $output=$send->reply($replyToken,$message);
            }
            if($message['type']=='text' and $db->counter==5){
              $phone=preg_replace('/[^0-9]/', '', $pesan_datang);
              if($phone!=0){
                $data['contact']=$phone;
                $sql=$this->Dbs->update($condition,$data,"jubalapak");
                if($sql){
                  $dataUser['flag']='default';
                  $dataUser['counter']=0;
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$dataUser,'user');
                  if($sql){
                    $message=array($send->text("jubalapak berhasil terdaftar! tim sumbangin akan segera melakukan verifikasi kebaikan yang kamu kirim nih! ditunggu ya.."));
                    $output=$send->reply($replyToken,$message);
                  }
                }

              }else{
                $message=array($send->text("Ups sepertinya input yang kamu masukan bukan angka"));
                $output=$send->reply($replyToken,$message);
              }
            }

          }
          else if(substr($db->flag,0,9)=='sumbangin'){
            require_once('class/Imagemap.php');
            $imagemap=new Imagemap();
            $flagdata=explode("#",$db->flag);
            $table=$flagdata[1];
            $id=$flagdata[2];
            $condition=array('id_user'=>$userId,'id_things'=>$id);

            if($message['type']=='text' and $db->counter==1 and $upPesan !='MULAI SUMBANGIN'){
              $data['contents']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,$table."_things");
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $messages=[];
                $msg1=$send->text("Oke! kira kira berapa banyak sumbanganmu jika dalam ukuran kardus dibawah ini?");
                $msg2=$imagemap->box();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }
            }
            if($db->counter==2){//menentukan ukuran kardus
              if($upPesan=="S@BOX" || $upPesan =="M@BOX" || $upPesan =="L@BOX" || $upPesan =="XL@BOX"){
                $upPesanData=explode("@",$upPesan);
                $size=$upPesanData[0];
                $data['size']=$size;
                $sql=$this->Dbs->update($condition,$data,$table."_things");
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $messages=[];
                  $msg1=$send->text("untuk mempermudah Sumbangin, sertakan foto barang sumbangannya ya");
                  $msg2=$imagemap->upload_foto();
                  array_push($messages,$msg1,$msg2);
                  $output=$send->reply($replyToken,$messages);
                }

              }else{
                $message=array($send->text("Ups! Tentukan dulu ukuran sumbangan kamu untuk melanjutkan pengisian data kebaikan"));
                $output=$send->reply($replyToken,$message);
              }

            }
            if ($message['type']=='image' and $db->counter==3){
              $imgprocess=$this->getMessage($messageid);
              if($imgprocess!="error"){
                $data['imageUrl']=$imgprocess;
                $sql=$this->Dbs->update($condition,$data,$table."_things");
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $messages=[];
                  $msg1=$send->text("Foto berhasil disimpan!,dimana lokasi barang ini bisa diambil?");
                  $msg2=$imagemap->lokasi();
                  array_push($messages,$msg1,$msg2);
                  $output = $send->reply($replyToken,$messages);
                }
              }else{
                $pre=array($send->text("Gagal Upload,Pastikan Ukuran Foto Tidak Terlalu Besar"));
                $output=$send->reply($replyToken,$pre);
              }

            }else if($message['type']=='text' and $db->counter==3){
              $message=array($send->text("Ups upload dulu minimal 1 Foto untuk melanjutkan laporan"));
              $output=$send->reply($replyToken,$message);
            }

            if($message['type']=='location' and $db->counter==4){
              $data['latitude']=$latitude;
              $data['longitude']=$longitude;
              $sql=$this->Dbs->update($condition,$data,$table."_things");
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $message=array($send->text("oke terakhir nih kak, sertakan nomor telfon yang bisa dihubungi ya, balas chat ini dengan mengetik nomor telfon"));
                $output=$send->reply($replyToken,$message);
              }

            }else if($message['type']=='text' and $db->counter==4){
              $message=array($send->text("Ups! silahkan tentukan lokasi terlebih dahulu untuk menyelesaikan pengisian sumbangan"));
              $output=$send->reply($replyToken,$message);
            }

            if($message['type']=='text' and $db->counter==5){
              $phone=preg_replace('/[^0-9]/', '', $pesan_datang);
              if($phone!=0){
                $data['phone']=$phone;
                $sql=$this->Dbs->update($condition,$data,$table."_things");
                if($sql){
                  $dataUser['flag']='default';
                  $dataUser['counter']=0;
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$dataUser,'user');
                  if($sql){
                    $message=array($send->text("sumbangan berhasil tercatat! tim sumbangin akan segera melakukan verifikasi kebaikan yang kamu kirim nih! ditunggu ya.."));
                    $output=$send->reply($replyToken,$message);
                  }
                }

              }else{
                $message=array($send->text("Ups sepertinya input yang kamu masukan bukan angka"));
                $output=$send->reply($replyToken,$message);
              }
            }


          }
          else if(substr($db->flag,0,8)=='campaign'){
            $getpostdata=explode("#",$db->flag);
            $id_campaign=$getpostdata[1];
            $condition=array('id_user'=>$userId,'id_campaign'=>$id_campaign);
            if($message['type']=='text' and $db->counter==1){
              $data['name']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'campaign');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $message=array($send->text("Oke! bisa jelaskan bagaimana deskripsi sumbangan ini butuh apa saja?"));
                $output=$send->reply($replyToken,$message);
              }
            }
            if($message['type']=='text' and $db->counter==2){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $data['desc']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'campaign');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $messages=[];
                $msg1=$send->text("untuk mempermudah Sumbangin, silakan sertakan foto/poster kampanye kamu");
                $msg2=$imagemap->upload_foto();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }
            }

            if ($message['type']=='image' and $db->counter==3){
              $imgprocess=$this->getMessage($messageid);
              if($imgprocess!="error"){
                $data['name']=$imgprocess;
                $data['id_campaign']=$id_campaign;

                $sql=$this->Dbs->insert($data,'campaign_image');
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $buttons = [];
                  $button1 = array(
                    'type' => 'datetimepicker',
                    'label' => 'Tentukan Waktu',
                    'data' => 'datetime',
                    'mode' => 'datetime',
                    'initial' => date("Y-m-d").'t00:00',
                    'max' => '2100-12-31T23:59',
                    'min' => '1900-01-01T00:00',
                    'text' => '@TAPTIME'
                  );
                  array_push($buttons, $button1);
                  $messages = [];
                  $msg1 = $send->text('foto sudah disimpan!,kapan anda membuat gerakan ini?');
                  $msg2 = $send->buttonMessage(base_url()."xfile/images/waktumulai.jpg", "kapan?", "Kapan?", "klik button dibawah ini", $buttons);
                  array_push($messages, $msg1, $msg2);
                  $output = $send->reply($replyToken, $messages);
                }
              }else{
                $pre=array($send->text("Gagal Upload,Pastikan Ukuran Foto Tidak Terlalu Besar"));
                $output=$send->reply($replyToken,$pre);
              }
            }else if($db->counter==3){
              $message=array($send->text("Ups upload dulu minimal 1 Foto untuk melanjutkan laporan"));
              $output=$send->reply($replyToken,$message);
            }

            if ($postback['data'] == 'datetime' and $db->counter==4)
                {
                  require_once('class/Imagemap.php');
                  $imagemap=new Imagemap();
                $datapos = explode("T",$postback['params']['datetime']);
                $datetime=$datapos[0]." ".$datapos[1].":00";
                $data['datetime']=$datetime;
                $sql=$this->Dbs->update($condition,$data,'campaign');
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $ballons=[];
                  $ballon1=$send->text("Waktu berhasil dicatat!,dimana lokasi gerakan ini dibuat?");
                  $ballon2=$imagemap->lokasi();
                  array_push($ballons,$ballon1,$ballon2);
                  $output = $send->reply($replyToken,$ballons);
                }

              }else if($db->counter==4){
                  $message=array($send->text("Ups kamu belum menentukan kapan waktu terjadinya"));
                  $output=$send->reply($replyToken,$message);
                }

                //LOKASI
                if($message['type']=='location' and $db->counter==5){
                  require_once 'class/LocationHelper.php';
                  $location=new LocationHelper();
                  $city=$location->getCity($latitude,$longitude);
                  $data['latitude']=$latitude;
                  $data['longitude']=$longitude;
                  $data['city']=$city;

                  $sql=$this->Dbs->update($condition,$data,'campaign');
                  if($sql){
                    $this->Dbs->add_counter($userId,$db->counter);
                    $message=array($send->text("okay terakhir! sertakan nomor telfon untuk kontak buka sumbangan, balas chat ini dengan mengetik nomor telfon"));
                    $output=$send->reply($replyToken,$message);
                  }
                }else if($message['type']=='text' and $db->counter==5){
                  $message=array($send->text("Ups! silahkan tentukan lokasi terlebih dahulu untuk menyelesaikan pengisian buka sumbangan"));
                  $output=$send->reply($replyToken,$message);
                }

                if($message['type']=='text' and $db->counter==6){
                  $phone=preg_replace('/[^0-9]/', '', $pesan_datang);
                  if($phone!=0){
                    $data['contact']=$phone;

                    $sql=$this->Dbs->update($condition,$data,'campaign');
                    if($sql){
                      $set['flag']='default';
                      $set['counter']=0;
                      $this->Dbs->update(array('id_user'=>$userId),$set,'user');
                      $messages=[];
                      $msg1=$send->text("Kampanye berhasil tercatat dan akan terpublish setelah diverifikasi admin,tim sumbangin akan segera memberikan info terkait kampanye anda");
                      $msg2=$send->text("Untuk melihat laporan anda silahkan ketik PROFIL atau cek Menu Profil");
                      array_push($messages,$msg1,$msg2);
                      $output=$send->reply($replyToken,$messages);
                    }

                  }else{
                    $message=array($send->text("Ups sepertinya input yang kamu masukan bukan angka"));
                    $output=$send->reply($replyToken,$message);
                  }
                }

                //LOKASI
          }
          else if(substr($db->flag,0,7)=='gogreen'){
            $getpostdata=explode("#",$db->flag);
            $id_campaign=$getpostdata[1];
            $condition=array('id_user'=>$userId,'id_gogreen'=>$id_campaign);
            if($message['type']=='text' and $db->counter==1){
              $data['name']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'gogreen');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $message=array($send->text("Oke! bisa jelaskan bagaimana deskripsi gerakan gogreen ini"));
                $output=$send->reply($replyToken,$message);
              }
            }
            if($message['type']=='text' and $db->counter==2){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $data['desc']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'gogreen');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $messages=[];
                $msg1=$send->text("untuk mempermudah Sumbangin, silakan sertakan foto/poster kampanye kamu");
                $msg2=$imagemap->upload_foto();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }
            }

            if ($message['type']=='image' and $db->counter==3){
              $imgprocess=$this->getMessage($messageid);
              if($imgprocess!="error"){
                $data['name']=$imgprocess;
                $data['id_gogreen']=$id_campaign;

                $sql=$this->Dbs->insert($data,'gogreen_image');
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $buttons = [];
                  $button1 = array(
                    'type' => 'datetimepicker',
                    'label' => 'Tentukan Waktu',
                    'data' => 'datetime',
                    'mode' => 'datetime',
                    'initial' => date("Y-m-d").'t00:00',
                    'max' => '2100-12-31T23:59',
                    'min' => '1900-01-01T00:00',
                    'text' => '@TAPTIME'
                  );
                  array_push($buttons, $button1);
                  $messages = [];
                  $msg1 = $send->text('foto sudah disimpan!,kapan anda membuat gerakan ini?');
                  $msg2 = $send->buttonMessage(base_url()."xfile/images/waktumulai.jpg", "kapan?", "Kapan?", "klik button dibawah ini", $buttons);
                  array_push($messages, $msg1, $msg2);
                  $output = $send->reply($replyToken, $messages);
                }
              }else{
                $pre=array($send->text("Gagal Upload,Pastikan Ukuran Foto Tidak Terlalu Besar"));
                $output=$send->reply($replyToken,$pre);
              }
            }else if($db->counter==3){
              $message=array($send->text("Ups upload dulu minimal 1 Foto untuk melanjutkan laporan"));
              $output=$send->reply($replyToken,$message);
            }

            if ($postback['data'] == 'datetime' and $db->counter==4)
                {
                  require_once('class/Imagemap.php');
                  $imagemap=new Imagemap();
                $datapos = explode("T",$postback['params']['datetime']);
                $datetime=$datapos[0]." ".$datapos[1].":00";
                $data['datetime']=$datetime;
                $sql=$this->Dbs->update($condition,$data,'gogreen');
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $ballons=[];
                  $ballon1=$send->text("Waktu berhasil dicatat!,dimana lokasi gerakan ini dibuat?");
                  $ballon2=$imagemap->lokasi();
                  array_push($ballons,$ballon1,$ballon2);
                  $output = $send->reply($replyToken,$ballons);
                }

              }else if($db->counter==4){
                  $message=array($send->text("Ups kamu belum menentukan kapan waktu terjadinya"));
                  $output=$send->reply($replyToken,$message);
                }

                //LOKASI
                if($message['type']=='location' and $db->counter==5){
                  require_once 'class/LocationHelper.php';
                  $location=new LocationHelper();
                  $city=$location->getCity($latitude,$longitude);
                  $data['latitude']=$latitude;
                  $data['longitude']=$longitude;
                  $data['city']=$city;

                  $sql=$this->Dbs->update($condition,$data,'gogreen');
                  if($sql){
                    $this->Dbs->add_counter($userId,$db->counter);
                    $message=array($send->text("okay terakhir! sertakan nomor telfon untuk kontak buka sumbangan, balas chat ini dengan mengetik nomor telfon"));
                    $output=$send->reply($replyToken,$message);
                  }
                }else if($message['type']=='text' and $db->counter==5){
                  $message=array($send->text("Ups! silahkan tentukan lokasi terlebih dahulu untuk menyelesaikan pengisian buka sumbangan"));
                  $output=$send->reply($replyToken,$message);
                }

                if($message['type']=='text' and $db->counter==6){
                  $phone=preg_replace('/[^0-9]/', '', $pesan_datang);
                  if($phone!=0){
                    $data['contact']=$phone;

                    $sql=$this->Dbs->update($condition,$data,'gogreen');
                    if($sql){
                      $set['flag']='default';
                      $set['counter']=0;
                      $this->Dbs->update(array('id_user'=>$userId),$set,'user');
                      $messages=[];
                      $msg1=$send->text("Kampanye berhasil tercatat dan akan terpublish setelah diverifikasi admin,tim sumbangin akan segera memberikan info terkait kampanye anda");
                      $msg2=$send->text("Untuk melihat laporan anda silahkan ketik PROFIL atau cek Menu Profil");
                      array_push($messages,$msg1,$msg2);
                      $output=$send->reply($replyToken,$messages);
                    }

                  }else{
                    $message=array($send->text("Ups sepertinya input yang kamu masukan bukan angka"));
                    $output=$send->reply($replyToken,$message);
                  }
                }

                //LOKASI
          }
          else{

            //Buat SUMBANGAN
            if (substr($upPesan,0,10)=='!SUMBANGAN' and $groupId==null){
              $datapost=explode("@",$pesan_datang);
              $category=$datapost[1];
              if($db->phone==null){
                $data['flag']='phoneUpdate';
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                if($sql){
                  $message=array($send->text("Ups! kamu belum mengatur nomor yang bisa dihubungi, silahkan balas bot ini dengan nomor telfon kamu"));
                  $output=$send->reply($replyToken,$message);
                  $client->replyMessage($output);
                }
              }else{
                $dataCampaign=array(
                  'category'=>$category,
                  'id_user'=>$userId,
                );
                $sql=$this->Dbs->insert($dataCampaign,'campaign');
                if($sql){
                  $recent_id=$this->db->insert_id();//id yang digenerate oleh Auto Increment
                  $data['flag']='campaign#'.$recent_id;
                  $data['counter']=1;
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    //line://oaMessage/@qtr1343p/?ganti_textini
                    $message=array($send->text("Apa nama gerakan yang akan kamu buat?"));
                    $output=$send->reply($replyToken,$message);
                  }
                }
              }
            }

            if (substr($upPesan,0,14)=='!BUAT GO GREEN' and $groupId==null){
              $datapost=explode("@",$pesan_datang);
              $category=$datapost[1];
              if($db->phone==null){
                $data['flag']='phoneUpdate';
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                if($sql){
                  $message=array($send->text("Ups! kamu belum mengatur nomor yang bisa dihubungi, silahkan balas bot ini dengan nomor telfon kamu"));
                  $output=$send->reply($replyToken,$message);
                  $client->replyMessage($output);
                }
              }else{
                $dataCampaign=array(
                  'category'=>$category,
                  'id_user'=>$userId,
                );
                $sql=$this->Dbs->insert($dataCampaign,'gogreen');
                if($sql){
                  $recent_id=$this->db->insert_id();//id yang digenerate oleh Auto Increment
                  $data['flag']='gogreen#'.$recent_id;
                  $data['counter']=1;
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    //line://oaMessage/@qtr1343p/?ganti_textini
                    $message=array($send->text("Apa nama gerakan yang akan kamu buat?"));
                    $output=$send->reply($replyToken,$message);
                  }
                }
              }
            }
            //END Buat Sumbangan

            //RICH MENU RECEIVER
            if ($upPesan=='!BUKA SUMBANGAN'){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $messages=[];
              $msg1=$send->text("Mau buat sumbangan apa nih? Tap Aja");
              $msg2=$imagemap->buka_sumbangan();
              array_push($messages,$msg1,$msg2);
              $output=$send->reply($replyToken,$messages);
            }
            if (substr($upPesan,0,3)=='JBL'){//ketika imagemap jubalapak di klik dan memulai jual barang
              $datapost=explode("@",$pesan_datang);
              $category=$datapost[1];
              $condition=array('id_user'=>$userId,'category'=>$category);
              if($db->phone==null){
                $data['flag']='phoneUpdate';
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                if($sql){
                  $message=array($send->text("Ups! kamu belum mengatur nomor yang bisa dihubungi, silahkan balas bot ini dengan nomor telfon kamu"));
                  $output=$send->reply($replyToken,$message);
                  $client->replyMessage($output);
                }
              }else{
                $dataJubalapak=array(
                  'category'=>$category,
                  'id_user'=>$userId,
                );
                $sql=$this->Dbs->insert($dataJubalapak,'jubalapak');
                if($sql){
                  $recent_id=$this->db->insert_id();//id yang digenerate oleh Auto Increment
                  $data['flag']='jubalapak#'.$recent_id;
                  $data['counter']=1;
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    //line://oaMessage/@qtr1343p/?ganti_textini
                    $message=array($send->text("Nama Barangnya apa nih?"));
                    $output=$send->reply($replyToken,$message);
                  }
                }
              }
          }
            if ($upPesan=='!JUBALAPAK CREATE'){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $messages=[];
              $msg1=$send->text("Mau jubalapak apa nih? Tap Aja");
              $msg2=$imagemap->jubalapak_category();
              array_push($messages,$msg1,$msg2);
              $output=$send->reply($replyToken,$messages);
            }
            if ($upPesan=='!JUBALAPAK'){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $messages=[];
              $msg1=$send->text("Udah cek jubalapak hari ini?");
              $msg2=$imagemap->jubalapak();
              array_push($messages,$msg1,$msg2);
              $output=$send->reply($replyToken,$messages);
            }
            if ($upPesan=='!GO GREEN'){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $messages=[];
              $msg2=$imagemap->gogreen();
              array_push($messages,$msg2);
              $output=$send->reply($replyToken,$messages);
            }
            if ($upPesan=='!GO GREEN KAMPANYE'){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $messages=[];
              $msg1=$send->text("Mau buat gerakan apa nih? Tap Aja");
              $msg2=$imagemap->categorygogreen();
              array_push($messages,$msg1,$msg2);
              $output=$send->reply($replyToken,$messages);
            }
            if ($upPesan=='!PROFIL'){
              $buttons=[];
              $button1 = array('type'=>'uri','label'=>'Buka Profil','uri'=>base_url().'login/'.$userId);
              array_push($buttons,$button1);
              $ballons=[];
              $ballon1=$send->text("Kelola semua data di sumbangin? Tap aja");
              $ballon2=$send->buttonMessage("https://islamify.id/bot/sumbangin/xfile/images/dummy.jpg","Profil","Kelola Data","Tap Untuk Membuka Website",$buttons);
              array_push($ballons,$ballon1,$ballon2);
              $output = $send->reply($replyToken,$ballons);
            }
            //RICH MENU RECEIVER

            //FLEX Category
            if($upPesan=='!BUTUH SUMBANGAN'){
              require_once('class/Category.php');
              $category=new Category();
              $output=$send->reply($replyToken,$category->sumbangan());
            }
            if($upPesan=='!JUBALAPAK READY'){
              require_once('class/Category.php');
              $category=new Category();
              $output=$send->reply($replyToken,$category->jubalapak());
            }
            if($upPesan=='!LIHAT GO GREEN KAMPANYE'){
              require_once('class/Category.php');
              $category=new Category();
              $output=$send->reply($replyToken,$category->gogreen());
            }

            //FLEX Category


            //POSTBACK UNTUK MENERIMA LOKASI
            if (substr($postback['data'],0,8)=='readdesc'){
              $getpostdata=explode("#",$postback['data']);
              $table=$getpostdata[1];
              $id=$getpostdata[2];
              if($table=='campaign'){
                $condition=array('id_campaign'=>$id);
                $conditionImg=array('id_campaign'=>$id);
              }else{
                $condition=array('id_gogreen'=>$id);
                $conditionImg=array('id_gogreen'=>$id);

              }
              $loadDb=$this->Dbs->getdata($condition,$table);
              $check=$loadDb->num_rows();
              if($check>0){
                $getData=$loadDb->row();
                $messages=[];
                $loadImg=$this->Dbs->getdata($conditionImg,$table.'_image');
                if($loadImg->num_rows()>0){//check jika image ada didatabase
                  foreach ($loadImg->result() as $g) {
                    $image=base_url().'xfile/images/'.$g->name;
                    $msg2=$send->image($image);
                    array_push($messages,$msg2);
                  }
                  $msg1=$send->text($getData->desc);
                  array_push($messages,$msg1);
                }else{
                  $msg1=$send->text($getData->desc);
                  array_push($messages,$msg1);
                }
                $output=$send->reply($replyToken,$messages);

              }else{
                $message=array($send->text("Ups! Kampanye sudah tidak ada sumbangin"));
                $output=$send->reply($replyToken,$message);
              }
            }
            if (substr($postback['data'],0,4)=='goto'){
                $getpostdata=explode("#",$postback['data']);
                $address=$getpostdata[3];
                $lat=$getpostdata[1];
                $long=$getpostdata[2];
                $pre=array($send->location("Tap untuk melihat lokasi",$address,$lat,$long));
                $output=$send->reply($replyToken,$pre);
            }

            if (substr($postback['data'],0,8)=='category'){//category#'.$from.'#'.$title,
              require_once('class/Category.php');
              $category=new Category();
                $getpostdata=explode("#",$postback['data']);
                $table=$getpostdata[1];
                $ctg=$getpostdata[2];//nama kategori yang akan jadi parameter where
                if($table=='campaign'){
                  $output=$send->reply($replyToken,$category->itemSumbangan($ctg));
                }else if($table=='gogreen'){
                  $output=$send->reply($replyToken,$category->itemGogreen($ctg));
                }
            }
            if (substr($postback['data'],0,18)=='jubalapak_category'){//category#'.$from.'#'.$title,
              require_once('class/Category.php');
              $category=new Category();
                $getpostdata=explode("#",$postback['data']);
                $ctg=$getpostdata[1];//nama kategori yang akan jadi parameter where
                $output=$send->reply($replyToken,$category->itemJubalapak($ctg));
            }

            if (substr($postback['data'],0,9)=='sumbangin'){//'data' => 'sumbangin#'.$from."#$id",
              $getpostdata=explode("#",$postback['data']);
              $table=$getpostdata[1];
              if($table=='campaign'){
                $id_campaign=$getpostdata[2];
                $data=array(
                  'id_campaign'=>$id_campaign,
                  'id_user'=>$userId,
                );
              }else{
                $id_gogreen=$getpostdata[2];
                $data=array(
                  'id_gogreen'=>$id_gogreen,
                  'id_user'=>$userId,
                );
              }
              $sql=$this->Dbs->insert($data,$table.'_things');//ke table campaign_things atau gogreen_things
              if($sql){
                $message=array($send->text("Barang apa yang akan kamu sumbangkan?"));
                $output=$send->reply($replyToken,$message);
                $recent_id=$this->db->insert_id();
                $dataUser['flag']='sumbangin#'.$table.'#'.$recent_id;
                $dataUser['counter']=1;
                $this->Dbs->update(array('id_user'=>$userId),$dataUser,'user');
              }
              // $messages=array($send->text("table : $table \r\nid_campaign :$id_campaign"));
              // $output=$send->reply($replyToken,$messages);

            }
            //POSTBACK END
        }  //END ELSE DARI PENGECEKAN DB FLAG

        if ($upPesan == 'GET@MYID'){
              $pre=array($send->text($userId));
              $output=$send->reply($replyToken,$pre);
          }
        if ($upPesan == 'CEK') {
           if(!$reg->check($userId)){//pengecekan register
             $output=$send->reply($replyToken,$reg->message());
             $client->replyMessage($output);
             die;//proses tidak diteruskan jika kondisi ini terpenuhi
          }

          $ballons = [];
          $ballon1 = $send->text("Hasil : ");
          array_push($ballons, $ballon1);
          $output = $send->reply($replyToken, $ballons);

        }
        if ($upPesan == 'TAMBAH')//pemanggilan TEXT biasa
              {
                $angka1=5;
                $angka2=5;
                $hasil=$angka1+$angka2;
              $ballons = [];
              $ballon1 = $send->text("Hasil : ".$hasil);
              array_push($ballons, $ballon1);
              $output = $send->reply($replyToken, $ballons);
              }

        //CONTOH Message
        //FLEX Messagge

        if ($upPesan == '@@A')//pemanggilan TEXT biasa
              {
              $ballons = [];
              $ballon1 = $send->text("Text Pertama");
              $ballon2 = $send->text("Text Kedua");
              $ballon3 = $send->image("https://via.placeholder.com/450x400");

              array_push($ballons, $ballon1, $ballon2,$ballon3);
              $output = $send->reply($replyToken, $ballons);
              }
          if ($upPesan == '@@B')//pemanggilan button Template
              {
              $buttons=[];
              $button1 = array('type'=>'postback','label'=>'Test1','data'=>'lol');
              $button2 = array('type'=>'postback','label'=>'Test2','data'=>'lol');
              array_push($buttons,$button1,$button2);
              $ballons=[];
              $ballon1=$send->text("ini template");
              $ballon2=$send->buttonMessage("https://via.placeholder.com/450x400","ini alt","ini title","ini caption",$buttons);
              array_push($ballons,$ballon1,$ballon2);
              $output = $send->reply($replyToken,$ballons);
              }
          if ($upPesan == '@@C')//pemanggilan Confirm Template
              {
              $buttons=[];
              $button1 = array('type'=>'postback','label'=>'Test1','data'=>'lol');
              $button2 = array('type'=>'postback','label'=>'Test2','data'=>'lol');
              array_push($buttons,$button1,$button2);
              $ballons=[];
              $ballon1=$send->text("ini template");
              $ballon2=$send->confirmMessage("Alt","caption",$buttons);
              array_push($ballons,$ballon1,$ballon2);
              $output = $send->reply($replyToken,$ballons);
              }
          if ($upPesan == '@@D')//pemanggilan imageMap
              {
              $ballons=array($send->imagemap("https://islamify.id/dashboard/imagemap/mapfy/","ini alt"));//CONTOH REPLY 1 BALLON CHAT
              $output = $send->reply($replyToken,$ballons);
              }

//   $logs=json_encode($upPesan);
// $logsData['json']=$logs;
// $logsData['id_user']=$userId;
// $this->db->insert('logs_json', $logsData);
        $client->replyMessage($output);


  }

  function getMessage($idpesan){

  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.line.me/v2/bot/message/'.$idpesan.'/content',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "authorization: Bearer uLDiz75RqMSCaopYzxpmzpDQWmXORrfR4PKZx+OLfuiGvU7YuuxVFFogV6Q7X5bdJf3lQvvAn9heEifieyk8Z1dUf2VoDbvOmdfo6exevxzaYgekTSrfZk/Mxr2O3Bk76qQd1qy3l0rRvpUuAykomQdB04t89/1O/w1cDnyilFU=",
      "cache-control: no-cache",
      "postman-token: ca88e385-873e-ed46-2825-3c73eb879c63"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    return "error";
  } else {
    $filename=uniqid().'.jpeg';
    file_put_contents('xfile/images/'.$filename, $response);
    return $filename;
  }

}




}
